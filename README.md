# Unofficial PulseChain node setup guide
Made by @WildThingTV  ([Twitter](https://twitter.com/WildThing_TV), [Telegram](https://t.me/WildThingTV))

ver. 0.0.1


<h3 style="color: red;"><b>This guide is completely unofficial, and comes with no guarantees, or warranty. USE AT YOUR OWN RISK!</b></h3>

# Table of contents
- [Unofficial PulseChain node setup guide](#unofficial-pulsechain-node-setup-guide)
- [Table of contents](#table-of-contents)
- [Notes](#notes)
- [Recommended hardware](#recommended-hardware)
- [Setting up Docker (Engine)](#setting-up-docker-engine)
  * [Ubuntu](#ubuntu)
- [Setting up Docker (Desktop)](#setting-up-docker-desktop)
  * [Ubuntu-desktop](#ubuntu-desktop)
- [Clients](#clients)
  * [Execution layer](#execution-layer)
    + [Go-Pulse](#go-pulse)
  * [Consensus layer](#consensus-layer)
    + [Lighthouse-Pulse](#lighthouse-pulse)
- [Setup](#setup)
  * [Folder structure & JWT Key](#folder-structure-jwt-key)
  * [Network](#network)
  * [Execution client](#execution-client)
    + [Go-Pulse](#go-pulse-1)
      - [Geth port explanation](#geth-port-explanation)
      - [Go-Pulse parameters](#go-pulse-parameters)
    + [Docker parameters](#docker-parameters)
    + [Lighthouse-Pulse](#lighthouse-pulse-1)
      - [Lighthouse parameters](#lighthouse-parameters)
  * [Generating deposit keys via CLI](#validator-generate-keys-via-cli)
  * [Validator client](#validator-client)
    + [Lighthouse-Pulse](#lighthouse-pulse-2)
  * [Exit Validator](#exit-validator)
  * [Prometheus](#prometheus)
  * [Grafana](#grafana)
  * [Commands](#commands)
  * [Logs](#logs)
  * [Telegram](#telegram)


<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>


# Notes
This guide will be focusing on how to set up nodes with docker on linux. It outlines the steps

Also, this was designed to be read linearly, so to get the most out of this guide don't skip to a specific part, but rather read through it.

# Recommended hardware
For testnet:
* Modern CPU with 4+ **dedicated** cores
* 16GB+ RAM
* 1.5TB+ SSD Storage for a full-node
* 15TB+ SSD Storage for an archive node

For mainnet:
* Strong, modern CPU with 8+ **dedicated** cores
* 32GB+ RAM
* 3TB+ SSD Storage for a full-node
* 20TB+ SSD Storage for an archive node

Notes on running a validator:
* Only use dedicated hardware

Notes:
A VPS provider will often lend out a core to several different servers, and restrict how much of it you can use. Either make sure you are renting a server with dedicated cores, or rent/run a dedicated server.

# Setting up Docker (Engine)
## Ubuntu
```sh
# Remove any older versions of docker
sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update

# Install necessary packages for docker signing key
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg

# Setup docker signing key
sudo mkdir -m 0755 -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Install docker
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
# Setting up Docker (Desktop)
## Ubuntu-desktop
```sh
# Remove any older versions of docker
sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update

# Install necessary packages for docker signing key
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg

# Setup docker signing key
sudo mkdir -m 0755 -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Download latest DEB package
https://docs.docker.com/desktop/install/ubuntu/

# Install docker
sudo apt-get update
sudo apt-get install ./docker-desktop-<version>-<arch>.deb
```

# Clients
At the time of writing this, both the execution, and the consensus layer have two client options but we'll concentrate on two for now.

## Execution layer

### Go-Pulse
* Well tested, and widely used
* Extremely large archive node size (15TB+)
* Relatively slow to sync full node, and painfully slow to sync archive node
* Recommended by the official PulseChain dev team


## Consensus layer
We'll use Lighthouse for this guide

### Lighthouse-Pulse
* Security focused (External security reviews, and fuzzing)
* Written in Rust, which provides several safety guarantees, and great performance, when utilized correctly.

# Setup
We can begin the setup.

## Folder structure & JWT Key
I recommend you create a main directory for your nodes, somewhere on your filesystem, and then create 3 subdirectories titled `execution`, `consensus` and `wallet`.

In this scenario, I have decided that my main directory should be `/blockchain`.
```
mkdir -p /blockchain/execution /blockchain/consensus /blockchain/wallet
```

To generate the JWT key used for authentication, we're going to use openssl.

```sh
openssl rand -hex 32 | tr -d "\n" > /blockchain/jwt.hex
```

## Network
First, we create a docker network to provide internal communication between the consensus, and execution clients. Also need to specify subnet to assign custom ip addresses to containers in later steps.

```sh
sudo docker network create --subnet=172.18.0.0/16 pulsechain 
```


## Execution client
After creating the network, we can start our execution client.

### Go-Pulse
To set up Go-Pulse we can use these commands:
```sh
sudo docker pull registry.gitlab.com/pulsechaincom/go-pulse:latest

sudo docker run \
  -d \
  --restart unless-stopped \
  --network=pulsechain \
  --name=execution \
  -v /home/YOURUSERNAME/blockchain:/blockchain \
  -p 30303:30303 \
  -p 6060:6060 \
  -p 30303:30303/udp \
  registry.gitlab.com/pulsechaincom/go-pulse:latest \
  --pulsechain \
  --datadir=/blockchain/execution \
  --authrpc.addr "0.0.0.0" \
  --authrpc.vhosts "*" \
  --authrpc.jwtsecret /blockchain/jwt.hex \
  --metrics \
  --metrics.addr execution \
  --metrics.expensive

```
Make sure to replace the cache, syncmode, and volume with your own options. Details down below!

---

`\` characters at the end of the line are there to escape newlines, and allow for separating the parameters into several lines. This should be the last character at the end of each line, **except for the final line**.

---

#### Geth port explanation
| Port  | Protocol | Usage        |
|-------|----------|--------------|
| 30303 | TCP/UDP  | Node peering |
| 6060  | TCP/UDP  | Metrics      |

---

### Go-Pulse parameters

---

#### `--pulsechain`
Specifies that Go-Pulse should run with the PulseChain testnet v4 configuration.

---

##### `--datadir=/blockchain/execution`
Specifies that the `/blockchain/execution` directory should be used as the directory where we store 

---

#### `--authrpc.addr "0.0.0.0"`
Sets the authenticated RPC that geth uses to communicate with the consensus client to accept connections from any address.

**(In this case only internal addresses. Do not attempt to use this with `--network=host`, or when running outside a container!)**

---

#### `--authrpc.vhosts "*"`
Sets the authenticated RPC to accept requests regardless of the hostname the request is targeting.

---

#### `--authrpc.jwtsecret /blockchain/jwt.hex`
Sets the authenticated RPC to authenticate requests based on the shared JWT token we created earlier.

---

#### `--metrics`
Needed to get metrics showing in order to hook up Prometheus and Grafana.

---

#### `--metrics.addr execution`
Needed to get metrics showing in order to hook up Prometheus and Grafana. Use name of your execution client after `--metrics.addr`

---

#### `--metrics.expensive`
Needed to get metrics showing in order to hook up Prometheus and Grafana.

---


### Docker parameters

---

#### `-d `
Short for `--detached`. It makes the container run, and print its logs standalone from your terminal.

---

#### `--restart unless-stopped`
Relatively self-explanatory. If the container, or server crashes, the container will be restarted.

The only caveat is that the docker systemd service must be enabled on startup.
This is the default setting, but to make sure it's on, you can use
```
sudo systemctl enable docker
```

---

#### `--network=pulsechain`
We set the docker network of this container to the one we created previously.

Docker has name-based DNS resolution of containers inside the same network.
This is important because by default, containers get assigned random internal IP addresses, which change when the container is restarted/recreated.

By connecting the execution, consensus, and validator containers to the same network, we can avoid using ip addresses to link them together, instead we can specify the name we gave the container.

---

#### `--name=execution`
By naming the docker container, in docker commands we can refer to it by this name, instead of its identifier.

It also allows other containers in the same network to resolve its internal IP address by the name.

---

#### `-v /home/USERYOURUSER/blockchain:/blockchain`
`-v` is short for `--volume`. This parameter allows you to specify either a docker volume, or a location on your drive to mount into the container.

_(Note for advanced users: Mounting a symlink will not work)_

The syntax for the parameter is `-v <LOCATION_ON_DRIVE>:<LOCATION_IN_CONTAINER>`.

In this case, I use the command to mount my main directory (`/blockchain`) into the container.

If you named your main directory something else, the only part you'd want to change in the mount, is the `<LOCATION_ON_DRIVE>`.

---

#### `-p 30303:30303` and `-p 30303:30303/udp` and `-p 6060:6060`
`-p` is short for `--publish`. This parameter exposes port 30303 to the internet.

Go-Pulse uses this port by default to function. On TCP for listening, and on UDP for discovery.

The full syntax for this parameter is:

`HOST_BIND_ADDRESS:HOST_PORT:CONTAINER_TARGET_PORT`

For example, you could bind a container port on the host for access only locally with `-p 127.0.0.1:8545:8545`.

If you skip the HOST_BIND_ADDRESS, it will default to "0.0.0.0" which means all interfaces will be able to access it. (On a typical server this includes the internet.)

**IMPORTANT: If you skip the bind address, or specify 0.0.0.0, it will completely bypass any firewall on the system. More information [here](https://stackoverflow.com/questions/30383845/what-is-the-best-practice-of-docker-ufw-under-ubuntu/51741599#51741599).**

---

#### `registry.gitlab.com/pulsechaincom/go-pulse:latest`
This is the location of the go-pulse docker image that is provided by the PulseChain dev team. After the colon, we can specify a tag. This can either be a version, or in this case `latest`.

**After this parameter, all other parameters get passed to the geth command, instead of docker**

---


## Consensus client


### Lighthouse-Pulse
**IMPORTANT: Create container without metrics-address first, then get the container ip address (find it under [Commands](#commands) below). After getting the ip address, stop, delete container and recreate with ip address**

To run Lighthouse-Pulse we can use these commands:
```sh
sudo docker pull registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest

sudo docker run \
  -d \
	--network=pulsechain \
  --ip ASSIGNIPADDRESS \
  --restart unless-stopped \
	--name=consensus \
	-v /home/YOURUSERNAME/blockchain:/blockchain \
  -p 9000:9000/tcp \
  -p 9000:9000/udp \
  -p 5054:5054 \
	registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
  lighthouse bn \
	--metrics \
	--metrics-address=<CONTAINERIPADDRESS> \
	--network=pulsechain \
	--execution-jwt=/blockchain/jwt.hex \
	--datadir=/blockchain/consensus \
	--execution-endpoint=http://execution:8551 \
	--checkpoint-sync-url https://checkpoint.pulsechain.com \
  --http-address 0.0.0.0 \
  --http \
--suggested-fee-recipient="YOUR_ADDRESS_HERE"
```
[Docker parameters explained here](#docker-parameters)

### Lighthouse parameters
Some parameters used are explained [here](#go-pulse-parameters)

#### `-p 5054:5054`
Needed to get metrics showing in order to hook up Prometheus and Grafana.

---
#### `--metrics`
Needed to get metrics to show

---
#### `--slasher`
Needed to enable slasher

---
#### `--debug-level debug`
Needed to enable debug that's needed for slasher (as far as I understand)

---
#### `--metrics-address=CONTAINERIPADDRESS`
Needed to get metrics showing in order to hook up Prometheus and Grafana. Make sure to use your container ip address. Ideally you would use your container name or static ip address but I haven't been able to achieve that.

How to get container ip address explained [here](#-get-container-ip-address).

---

#### `--execution-endpoint`
Sets the execution client endpoint and port. You can use name of your execution client and the port number

More on how this works [here](#-networkpulsechain).

---

#### `--checkpoint-sync-url`
Defines the URL for consensus client quick syncing.

---

#### `--http-address 0.0.0.0`
Explained [here](#-authrpcaddr-0000).

---

#### `--http`
Enables the beacon node REST API.

---

#### `--boot-nodes`
Helps with peer finding. The address will be different for every chain (current address works for Pulsechain).

---

#### `--suggested-fee-recipient`
Enter your wallet address used for tips.

---

### Validator generate keys via CLI

```sh
# Navigate to root folder first

cd blockchain

git clone https://gitlab.com/pulsechaincom/staking-deposit-cli.git

cd staking-deposit-cli

sudo apt install python3-pip

sudo pip3 install -r requirements.txt

sudo python3 setup.py install

sudo ./deposit.sh install

sudo ./deposit.sh new-mnemonic --num_validators=1 --mnemonic_language=english --chain=pulsechain --folder=/home/wildthingtv/blockchain/validator-keys/ --eth1_withdrawal_address=YOURWITHDRAWALADDRESS
```

## Validator client
Strictly speaking, unless you are technically skilled enough to know how to run a validator, you probably shouldn't be running one.

I am publishing this section in order to rectify some other guides that have been floating around, that have been mostly comprised of copy & pasted commands, that most of the time, don't even work.

**Before running a validator, read all PulseChain documentation on staking PLS, and make sure you understand your responsibilities, and the serious risks that come with failing them.**

* [Validator FAQ](https://launchpad.v4.testnet.pulsechain.com/en/faq)
* [Validator checklist](https://launchpad.v4.testnet.pulsechain.com/en/checklist)
* [Validator overview, deposit, and key generation](https://launchpad.v4.testnet.pulsechain.com/en/overview)



### Lighthouse-Pulse
**IMPORTANT: Create container without metrics-address first, then get the container ip address (find it under [Commands](#commands) below). After getting the ip address, stop, delete container and recreate with ip address**

This command is used to import your keys. You will be asked for the password you created for the wallet in the steps above.
After you are done, you will need to stop the `validator_import` and then delete it. You can find commands [here](#commands)

```sh
# creating import validator to import keys
sudo docker run -it \
    --name validator_import \
    --net=pulsechain \
    -v /home/YOURUSERNAME/blockchain/validator_keys:/keys \
    -v /home/YOURUSERNAME/blockchain/wallet:/wallet \
    registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
    lighthouse \
    --network=pulsechain \
    account validator import \
    --directory=/keys \
    --datadir=/wallet
```
```sh
  sudo docker stop validator_import
  sudo docker rm validator_import
```
Replace `path_to_your_wallet_directory`, `Your graffiti here!`, and `suggested-fee-recipient` accordingly.
```sh
sudo docker pull registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest


  sudo docker run -d \
	--network=pulsechain \
  --ip ASSIGNIPADDRESS \
  --restart unless-stopped \
	--name=validator \
    -v /home/YOURUSERNAME/blockchain/wallet:/wallet \
     -p 5064:5064 \
	registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
  lighthouse vc \
  	--metrics \
	--metrics-address=CONTAINERIPADDRESS \
	--network=pulsechain \
  --http-address "0.0.0.0" \
  --http \
  --unencrypted-http-transport \
  --http-allow-origin "*" \
  --beacon-nodes "http://consensus:5052" \
	--graffiti "Your graffiti here!" \
  --suggested-fee-recipient "YOUR_ADDRESS_HERE" \
  --validators-dir /wallet/validators
```
## Exit Validator
** IMPORTANT: This action is irreversable. Be very careful!
 Reference document: https://lighthouse-book.sigmaprime.io/voluntary-exit.html
 Exit phrase: Exit my validator
 
```sh
sudo docker run -it \
 --net=pulsechain \
 --name=validator-exit \
 -v /home/wildthingtv/blockchain:/blockchain \
 registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest \
 lighthouse \
  --network pulsechain \
  account validator exit \
  --keystore /blockchain/validator_keys/YOURKEYSTOREFILENAME \
  --beacon-node http://consensus:5052
```

## Prometheus
Installing prometheus in docker

```sh
 sudo docker run -d \
    --name prometheus \
    --network=pulsechain \
    -p 9090:9090 \
    -v /home/YOURUSERNAME/blockchain/prometheus.yml:/etc/prometheus/prometheus.yml \
    prom/prometheus 

```
## Grafana
Installing grafana in docker: https://www.coincashew.com/coins/overview-eth/guide-or-how-to-setup-a-validator-on-eth2-mainnet/part-i-installation/monitoring-your-validator-with-grafana-and-prometheus
** IMPORTANT: When pointed Grafana to Prometheus use http://<Prometheus container IP address>:9090

```sh
 sudo docker run -d \
 --name grafana \
 --network=pulsechain \
 -p 3000:3000 grafana/grafana-oss  
```
## Commands
Miscelaneous commands used during the process

```sh
# stop any running containers
sudo docker stop execution
sudo docker stop consensus
sudo docker stop validator
```
```sh
# Remove already existing containers
sudo docker rm execution
sudo docker rm consensus
sudo docker rm validator
```
```sh
# Pull latest updates
sudo docker pull registry.gitlab.com/pulsechaincom/go-pulse:latest
sudo docker pull registry.gitlab.com/pulsechaincom/lighthouse-pulse:latest
```

### Get container IP address
```sh
# To view all active conatiners and their information
sudo docker ps --format \
"table {{.ID}}\t{{.Status}}\t{{.Names}}"
```
```sh
# To get IP Address of docker container via container id copied from using command above
sudo docker inspect -f \
'{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
 entercontainerid
```

```sh
# Run this command in order to get Ledger to connect via usb
wget -q -O - https://raw.githubusercontent.com/LedgerHQ/udev-rules/master/add_udev_rules.sh | sudo bash
# Run this command to be able to run AppImage (Ledger) as executable 
sudo apt install libfuse2
```

```sh
//Open up firewall ports
sudo ufw allow 30303
sudo ufw allow 12000
sudo ufw allow 13000
sudo ufw allow 3000
sudo ufw allow 9090
sudo ufw allow 4000
```
```sh
//Update
sudo apt-get update -y && sudo apt-get upgrade -y
sudo apt-get dist-upgrade -y && sudo apt autoremove -y
```
```sh
//Install/update needed programs
sudo apt-get install \
apt-transport-https \
ca-certificates \
curl \
gnupg \
git \
ufw \
openssl \
lsb-release
```

```sh
# To disable container restart
sudo docker update --restart=no $(docker ps -a -q)
sduo docker update --restart=no the-container-you-want-to-disable-restart
```

```sh
# Follow node logs
sudo docker logs --follow execution
sudo docker logs --follow consensus
sudo docker logs --follow validator
```
```sh
# To start docker desktop on startup
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```

```sh
# To disable start docker desktop on startup
sudo systemctl disable docker.service
sudo systemctl disable containerd.service

```

```sh
# To force remove locked folders
sudo rm -rf folder_name

```
```sh
# To list all containers
docker ps -a
```
```sh
# To view container stats
sudo docker stats
```


```sh
# To run system usage 
htop
```

```sh
# To check temps
sudo apt-get install lm-sensors 
sudo sensors-detect
sudo service kmod start
sensors
```

### Use following to pull from repo docker
---

```sh
sudo docker pull image:latest
```

```sh
# Dockers default behavior is to check for an updated version of an existing tag next time you run it.
# You can clean up old images by running docker image prune
# This command will list all your existing images in repo:tag format:

sudo docker image ls | awk '{print $1":"$2}'
```

```sh
# And you can combine that with a pull if you want to update those:
sudo docker image ls | tail -n +2 | awk '{print $1":"$2}' | xargs -L1 docker pull
```
---

```sh
# To unlock locked folders
sudo chmod 777 -R foldername
```

```sh
# To remove docker reference https://askubuntu.com/questions/935569/how-to-completely-uninstall-docker
dpkg -l | grep -i docker
sudo apt-get purge -y docker-engine docker docker.io docker-ce docker-ce-cli docker-compose-plugin
sudo apt-get autoremove -y --purge docker-engine docker docker.io docker-ce docker-compose-plugin
```
```sh
# To install ncdu app to view what uses the storage. Navigate to root and type ncdu
sudo apt install ncdu
```

```sh
# Navigate to home
cd /
```
```sh
# Navigate to root
cd /
```

## Logs

```sh
# Trancate logs
truncate -s 0 /var/log/syslog
truncate -s 0 /var/log/kern.log
```
```sh
# Restart the syslog service (use one or the other)
systemcl restart syslog
service syslog restart
```
```sh
# To force logs to rotate and delete automatically (add maxsize 1G)
sudo nano /etc/logrotate.d/rsyslog
```

## Telegram

```sh
# To install Telegram
sudo apt update
sudo apt install telegram-desktop
```